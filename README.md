<h1 align="center">arrakis-python</h1>

<p align="center">Arrakis Python client library</p>

<p align="center">
  <a href="https://git.ligo.org/ngdd/arrakis-python/-/pipelines/latest">
    <img alt="ci" src="https://git.ligo.org/ngdd/arrakis-python/badges/main/pipeline.svg" />
  </a>
  <a href="https://ngdd.docs.ligo.org/arrakis-python/">
    <img alt="documentation" src="https://img.shields.io/badge/docs-mkdocs%20material-blue.svg?style=flat" />
  </a>
</p>

---

## Installation

```
pip install git+https://git.ligo.org/ngdd/arrakis-python.git
```

## Features

* Query live and historical timeseries data
* Describe channel metadata
* Search for channels matching a set of conditions
* Publish timeseries data

## Quickstart

### Stream timeseries

##### 1. Live data

``` python
import arrakis

channels = [
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    "H1:LSC-POP_A_LF_OUT_DQ",
]

for block in arrakis.stream(channels):
	print(block)
```

##### 2. Historical data

``` python
import arrakis

start = 1187000000
end = 1187001000
channels = [
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    "H1:LSC-POP_A_LF_OUT_DQ",
]

for block in arrakis.stream(channels, start, end):
	print(block)
```

### Fetch timeseries


``` python
import arrakis

start = 1187000000
end = 1187001000
channels = [
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    "H1:LSC-POP_A_LF_OUT_DQ",
]

block = arrakis.fetch(channels, start, end)
```

### Describe metadata

``` python
import arrakis

channels = [
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    "H1:LSC-POP_A_LF_OUT_DQ",
]

metadata = arrakis.describe(channels)
```

### Find channels

``` python
import arrakis

channels = arrakis.find("H1:LSC-*")
```

### Count channels

``` python
import arrakis

count = arrakis.count("H1:LSC-*")
```

### Publish timeseries

``` python
import arrakis
from arrakis import SeriesBlock, Time
import numpy

series = {
    "H1:FKE-TEST_CHANNEL1": numpy.array([0.1, 0.2, 0.3, 0.4], dtype=numpy.float64),
    "H1:FKE-TEST_CHANNEL2": numpy.array([1.1, 1.2, 1.3, 1.4], dtype=numpy.float64),
}
block = SeriesBlock(1234567890 * Time.SECONDS, series)

with arrakis.connect("kafka://localhost:9092") as client:
    client.publish(block)
```
