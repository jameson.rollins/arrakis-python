# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-python/-/raw/main/LICENSE

"""Clientless API access."""

from collections.abc import Iterator

import numpy

from . import constants
from .block import SeriesBlock
from .channel import Channel
from .client import Client


def fetch(channels: list[str], start: int, end: int) -> SeriesBlock:
    """Fetch timeseries data

    Parameters
    ----------
    channels : list[str]
        A list of channels to request.
    start : int
        The start time.
    end : int
        The end time.

    Returns
    -------
    SeriesBlock
        Series block with all channels requested.

    """
    with connect() as client:
        return client.fetch(channels, start, end)


def stream(
    channels: list[str], start: int | None = None, end: int | None = None
) -> Iterator[SeriesBlock]:
    """Stream live or offline timeseries data

    Parameters
    ----------
    channels : list[str]
        A list of channels to request.
    start : int, optional
        The start time, if specified.
    end : int, optional
        The end time, if specified.

    Yields
    ------
        SeriesBlock
            Series blocks with all channels requested.

    Setting neither start nor end begins a live stream
    starting from now.

    """
    with connect() as client:
        yield from client.stream(channels, start, end)


def describe(channels: list[str]) -> list[Channel]:
    """Get channel metadata for channels requested

    Parameters
    ----------
    channels : list[str]
        A list of channels to request.
    start : int, optional
        The start time, if specified.
    end : int, optional
        The end time, if specified.

    Returns
    -------
    list[Channel]
        Channel metadata, one per channel requested.

    """
    with connect() as client:
        return client.describe(channels)


def find(
    pattern: str = constants.DEFAULT_MATCH,
    data_type: list[numpy.dtype] | None = None,
    min_rate: int | None = constants.MIN_SAMPLE_RATE,
    max_rate: int | None = constants.MAX_SAMPLE_RATE,
) -> list[Channel]:
    """Find channels matching a set of conditions

    Parameters
    ----------
    pattern : str, optional
        A channel pattern to match channels with.
    data_type : list[numpy.dtype], optional
        Data types to match, if specified.
    min_rate : int, optional
        The minimum sampling rate for channels, if specified.
    max_rate : int, optional
        The maximum sampling rate for channels, if specified.

    Returns
    -------
    list[Channel]
        The list of channels which match.

    """
    with connect() as client:
        return client.find(pattern, data_type, min_rate, max_rate)


def count(
    pattern: str = constants.DEFAULT_MATCH,
    data_type: list[numpy.dtype] | None = None,
    min_rate: int | None = constants.MIN_SAMPLE_RATE,
    max_rate: int | None = constants.MAX_SAMPLE_RATE,
) -> int:
    """Count channels matching a set of conditions

    Parameters
    ----------
    pattern : str, optional
        A channel pattern to match channels with.
    data_type : list[numpy.dtype], optional
        Data types to match, if specified.
    min_rate : int, optional
        The minimum sampling rate for channels, if specified.
    max_rate : int, optional
        The maximum sampling rate for channels, if specified.

    Returns
    -------
    int
        The number of channels which match.

    """
    with connect() as client:
        return client.count(pattern, data_type, min_rate, max_rate)


def connect(url: str | None = None) -> Client:
    """Return a connected instance of a client.

    Parameters
    ----------
    url : str, optional
        The Flight server to connect to.
        If not set, connect to a default server
        or one set by ARRAKIS_SERVER.

    Returns
    -------
    Client
        A connected instance of the client.

    """
    return Client(url)
