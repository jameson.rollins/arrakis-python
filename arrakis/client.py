# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-python/-/raw/main/LICENSE

"""Client-based API access."""

from __future__ import annotations

import os
from collections.abc import Iterator
from datetime import timedelta
from typing import Literal
from urllib.parse import urlparse

import numpy

from . import constants, pubsub
from .block import SeriesBlock
from .channel import Channel
from .traits import is_writable

DEFAULT_ARRAKIS_SERVER = "grpc://0.0.0.0:31206"


class NotSupportedError(Exception):
    pass


class Client:
    """Client to fetch or publish timeseries.

    Parameters
    ----------
    url : str, optional
        The URL to connect to.
        If set, protocol must be one of grpc:// or kafka://.

    The grpc:// protocol provides read-only access,
    while kafka:// provides write-only access.

    If the URL is not set, connect to a default server
    or one set by ARRAKIS_SERVER.

    """

    def __init__(self, url: str | None = None):
        if url is None:
            url = os.getenv("ARRAKIS_SERVER", DEFAULT_ARRAKIS_SERVER)
        assert url is not None

        # parse URL and connect to client
        parsed = urlparse(url)
        self._protocol = str(parsed.scheme)
        self._client: pubsub.SubscribingClient | pubsub.PubSubClient
        if pubsub.HAS_KAFKA:
            self._client = pubsub.PubSubClient(url)
        else:
            self._client = pubsub.SubscribingClient(url)

    def fetch(
        self,
        channels: list[str],
        start: int,
        end: int,
    ) -> SeriesBlock:
        """Fetch timeseries data

        Parameters
        ----------
        channels : list[str]
            A list of channels to request.
        start : int
            The start time.
        end : int
            The end time.

        Returns
        -------
        SeriesBlock
            Series block with all channels requested.

        """
        return self._client.fetch(channels, start, end)

    def stream(
        self,
        channels: list[str],
        start: int | None = None,
        end: int | None = None,
    ) -> Iterator[SeriesBlock]:
        """Stream live or offline timeseries data

        Parameters
        ----------
        channels : list[str]
            A list of channels to request.
        start : int, optional
            The start time, if specified.
        end : int, optional
            The end time, if specified.

        Yields
        ------
        SeriesBlock
            Series blocks with all channels requested.

        Raises
        ------
        NotSupportedError
            If the protocol does not support read access.

        Setting neither start nor end begins a live stream
        starting from now.

        """
        yield from self._client.stream(channels, start, end)

    def describe(self, channels: list[str]) -> list[Channel]:
        """Get channel metadata for channels requested

        Parameters
        ----------
        channels : list[str]
            A list of channels to request.

        Returns
        -------
        list[Channel]
            Channel metadata, one per channel requested.

        Raises
        ------
        NotSupportedError
            If the protocol does not support read access.

        """
        return self._client.describe(channels)

    def find(
        self,
        pattern: str = constants.DEFAULT_MATCH,
        data_type: list[numpy.dtype] | None = None,
        min_rate: int | None = constants.MIN_SAMPLE_RATE,
        max_rate: int | None = constants.MAX_SAMPLE_RATE,
    ) -> list[Channel]:
        """Find channels matching a set of conditions

        Parameters
        ----------
        pattern : str, optional
            A channel pattern to match channels with.
        data_type : list[numpy.dtype], optional
            Data types to match, if specified.
        min_rate : int, optional
            The minimum sampling rate for channels, if specified.
        max_rate : int, optional
            The maximum sampling rate for channels, if specified.

        Returns
        -------
        list[Channel]
            The list of channels which match.

        Raises
        ------
        NotSupportedError
            If the protocol does not support read access.

        """
        return self._client.find(pattern, data_type, min_rate, max_rate)

    def count(
        self,
        pattern: str = constants.DEFAULT_MATCH,
        data_type: list[numpy.dtype] | None = None,
        min_rate: int | None = constants.MIN_SAMPLE_RATE,
        max_rate: int | None = constants.MAX_SAMPLE_RATE,
    ) -> int:
        """Count channels matching a set of conditions

        Parameters
        ----------
        pattern : str, optional
            A channel pattern to match channels with.
        data_type : list[numpy.dtype], optional
            Data types to match, if specified.
        channel_type : list[ChannelType], optional
            Channel types to match, if specified.
        min_rate : int, optional
            The minimum sampling rate for channels, if specified.
        max_rate : int, optional
            The maximum sampling rate for channels, if specified.

        Returns
        -------
        int
            The number of channels which match.

        Raises
        ------
        NotSupportedError
            If the protocol does not support read access.

        """
        return self._client.count(pattern, data_type, min_rate, max_rate)

    def register(
        self,
        producer_id: str,
        source: str,
        scope: str = "*",
    ):
        if not is_writable(self._client):
            raise NotSupportedError(
                "client does not have publish functionality, install the publish extra"
            )
        self._client.register(producer_id, source, scope)

    def publish(
        self, block: SeriesBlock, timeout: timedelta = constants.DEFAULT_TIMEOUT
    ) -> None:
        """Publish timeseries data

        Parameters
        ----------
        block : SeriesBlock
            A data block with all channels to publish.
        timeout : timedelta, optional
            The maximum time to wait to publish before timing out.
            Default is 2 seconds.

        Raises
        ------
        NotSupportedError
            If the protocol does not support write access.

        """
        if not is_writable(self._client):
            raise NotSupportedError(
                "client does not have publish functionality, install the publish extra"
            )
        self._client.publish(block, timeout=timeout)

    def close(self) -> None:
        """Close a client connection."""
        self._client.close()

    def __enter__(self) -> Client:
        return self

    def __exit__(self, *exc) -> Literal[False]:
        self.close()
        return False
