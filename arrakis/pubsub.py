# Copyright (c) 2023, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-python/-/raw/main/LICENSE

"""Publishing and subscribing client functionality."""

from __future__ import annotations

import itertools
import os
from collections.abc import Iterable, Iterator
from datetime import timedelta
from typing import Literal
from urllib.parse import urlparse

import numpy
import pyarrow
from pyarrow import flight

from . import constants
from .block import SeriesBlock, combine_blocks
from .channel import Channel
from .flight import (
    RequestType,
    channel_to_dtype_name,
    create_descriptor,
    serialize_batch,
)

try:
    from confluent_kafka import Producer
except ImportError:
    HAS_KAFKA = False
else:
    HAS_KAFKA = True


DEFAULT_ARRAKIS_SERVER = "grpc://0.0.0.0:31206"


class SubscribingClient:
    """Subscribing client to access timeseries.

    Parameters
    ----------
    url: str
        The URL to connect to.

    """

    def __init__(self, url: str):
        if url is None:
            url = os.getenv("ARRAKIS_SERVER", DEFAULT_ARRAKIS_SERVER)
        assert url is not None

        # parse URL and create consumer
        parsed = urlparse(url)
        if parsed.scheme != "grpc":
            raise ValueError("invalid URL: must start with grpc://")

        self._client = flight.connect(url)

    def fetch(
        self,
        channels: list[str],
        start: int,
        end: int,
    ) -> SeriesBlock:
        """Fetch timeseries data

        Parameters
        ----------
        channels : list[str]
            A list of channels to request.
        start : int
            The start time.
        end : int
            The end time.

        Returns
        -------
        SeriesBlock
            Series block with all channels requested.

        """
        return combine_blocks(*self.stream(channels, start, end))

    def stream(
        self,
        channels: list[str],
        start: int | None = None,
        end: int | None = None,
    ) -> Iterator[SeriesBlock]:
        """Stream live or offline timeseries data

        Parameters
        ----------
        channels : list[str]
            A list of channels to request.
        start : int, optional
            The start time, if specified.
        end : int, optional
            The end time, if specified.

        Yields
        ------
        SeriesBlock
            Series blocks with all channels requested.

        Setting neither start nor end begins a live stream
        starting from now.

        """
        # set up flight info
        descriptor = create_descriptor(
            RequestType.Stream, channels=channels, start=start, end=end
        )
        flight_info = self._client.get_flight_info(descriptor)
        duration = 1 / 16.0  # FIXME: get duration from backend

        # yield record batches as they are received
        for chunk in self._client.do_get(flight_info.endpoints[0].ticket):
            yield SeriesBlock.from_column_batch(chunk.data, duration)

    def describe(self, channels: list[str]) -> list[Channel]:
        """Get channel metadata for channels requested

        Parameters
        ----------
        channels : list[str]
            A list of channels to request.
        start : int, optional
            The start time, if specified.
        end : int, optional
            The end time, if specified.

        Returns
        -------
        list[Channel]
            Channel metadata, one per channel requested.

        """
        # set up flight info
        descriptor = create_descriptor(RequestType.Describe, channels=channels)
        flight_info = self._client.get_flight_info(descriptor)

        # get response, extract channel list
        return self._query_channel_metadata(flight_info)

    def find(
        self,
        pattern: str = constants.DEFAULT_MATCH,
        data_type: list[numpy.dtype] | None = None,
        min_rate: int | None = constants.MIN_SAMPLE_RATE,
        max_rate: int | None = constants.MAX_SAMPLE_RATE,
    ) -> list[Channel]:
        """Find channels matching a set of conditions

        Parameters
        ----------
        pattern : str, optional
            A channel pattern to match channels with.
        data_type : list[numpy.dtype], optional
            Data types to match, if specified.
        min_rate : int, optional
            The minimum sampling rate for channels, if specified.
        max_rate : int, optional
            The maximum sampling rate for channels, if specified.

        Returns
        -------
        list[Channel]
            The list of channels which match.

        """
        # set up flight info
        descriptor = create_descriptor(
            RequestType.Find,
            pattern=pattern,
            data_type=data_type,
            min_rate=min_rate,
            max_rate=max_rate,
        )
        flight_info = self._client.get_flight_info(descriptor)

        # get response, extract channel list
        return self._query_channel_metadata(flight_info)

    def count(
        self,
        pattern: str = constants.DEFAULT_MATCH,
        data_type: list[numpy.dtype] | None = None,
        min_rate: int | None = constants.MIN_SAMPLE_RATE,
        max_rate: int | None = constants.MAX_SAMPLE_RATE,
    ) -> int:
        """Count channels matching a set of conditions

        Parameters
        ----------
        pattern : str, optional
            A channel pattern to match channels with.
        data_type : list[numpy.dtype], optional
            Data types to match, if specified.
        min_rate : int, optional
            The minimum sampling rate for channels, if specified.
        max_rate : int, optional
            The maximum sampling rate for channels, if specified.

        Returns
        -------
        int
            The number of channels which match.

        """
        # set up flight info
        descriptor = create_descriptor(
            RequestType.Count,
            pattern=pattern,
            data_type=data_type,
            min_rate=min_rate,
            max_rate=max_rate,
        )
        flight_info = self._client.get_flight_info(descriptor)

        # get response, extract count
        reader = self._client.do_get(flight_info.endpoints[0].ticket)
        return sum(reader.read_all().column("count").to_pylist())

    def close(self) -> None:
        self._client.close()

    def __enter__(self) -> SubscribingClient:
        return self

    def __exit__(self, *exc) -> Literal[False]:
        self.close()
        return False

    def _query_channel_metadata(self, flight_info: flight.FlightInfo) -> list[Channel]:
        """Retrieve channel metadata corresponding to a FlightInfo object."""
        channels = []
        reader = self._client.do_get(flight_info.endpoints[0].ticket)
        for channel_meta in reader.read_all().to_pylist():
            channel = Channel.from_name(
                channel_meta["channel"],
                data_type=channel_meta["data_type"],
                sample_rate=channel_meta["sample_rate"],
                partition_id=channel_meta["partition_id"],
            )
            channels.append(channel)
        return channels


class PubSubClient(SubscribingClient):
    """Client to access and publish timeseries.

    Parameters
    ----------
    url : str
        The URL to connect to.

    """

    def __init__(self, url: str):
        super().__init__(url)
        if not HAS_KAFKA:
            raise ImportError(
                "Publishing client requires confluent-kafka to be installed."
                "This is provided by the 'publish' extra or it can be "
                "installed manually through pip or conda."
            )
        self._producer: Producer

        # validation checks for publishing
        self._channels: set[Channel] = set()
        self._source: str

        # registry
        self._registered = False
        self._producer_id: str
        self._partitions: dict[str, str]

    def register(
        self,
        producer_id: str,
        source: str,
        scope: str = "*",
    ):
        assert not self._registered, "has already registered"

        # set up producer
        self._source = source
        self._producer_id = producer_id
        properties = self._get_connection_properties(producer_id)
        self._producer = Producer(
            {
                "message.max.bytes": 10_000_000,  # 10 MB
                "enable.idempotence": True,
                **properties,
            }
        )

        # query for the current partition mapping
        metadata = self.find(f"{self._source}:{scope}")
        self._partitions = {}
        for channel in metadata:
            if channel.partition_id:
                self._partitions[channel.name] = channel.partition_id

        self._registered = True

    def publish(
        self,
        block: SeriesBlock,
        timeout: timedelta = constants.DEFAULT_TIMEOUT,
    ) -> None:
        """Publish timeseries data

        Parameters
        ----------
        block : SeriesBlock
            A data block with all channels to publish.
        timeout : timedelta, optional
            The maximum time to wait to publish before timing out.
            Default is 2 seconds.

        """
        assert self._registered, "client needs to register prior to publishing data"

        # check for new metadata changes
        channels = set(block.channels)
        if not channels == self._channels:
            # ensure that all channels correspond to a single source
            sources = {channel.source for channel in block.channels}
            if len(sources) > 1:
                raise ValueError(
                    "Blocks must only consist of channels from a single source"
                )
            source = sources.pop()
            if source != self._source:
                raise ValueError(
                    "published channel source does not match registered source"
                )
            changed = channels - self._channels

            # exchange to transfer metadata and get new/updated partition IDs
            if changed:
                self._update_partitions(changed)

            # update tracked metadata
            self._channels = channels

        # publish data for each data type, splitting into
        # subblocks based on a maximum channel maximum
        for partition_id, batch in block.to_row_batches(self._partitions):
            self._publish_data(partition_id, batch)

    def _publish_data(self, partition_id: str, batch: pyarrow.RecordBatch) -> None:
        """Publish data for a given data type."""
        topic = f"arrakis-{partition_id}"
        self._producer.produce(topic=topic, value=serialize_batch(batch))

    def _publish_metadata(self, time: int, metadata: set[Channel]) -> None:
        """Publish metadata for a set of channels."""
        topic = f"arrakis-metadata-{self._source}_{self._producer_id}"
        packet = "\n".join([channel.to_json(time) for channel in metadata])
        self._producer.produce(topic=topic, value=packet)

    def _update_partitions(self, channels: Iterable[Channel]) -> None:
        # set up flight
        assert self._registered, "has not registered yet"
        descriptor = create_descriptor(
            RequestType.Partition,
            producer_id=self._producer_id,
        )
        writer, reader = self._client.do_exchange(descriptor)

        # send over list of channels to map new/updated partitions for
        dtypes = [channel_to_dtype_name(channel) for channel in channels]
        schema = pyarrow.schema(
            [
                pyarrow.field("channel", pyarrow.string(), nullable=False),
                pyarrow.field("data_type", pyarrow.string(), nullable=False),
                pyarrow.field("sample_rate", pyarrow.int32(), nullable=False),
                pyarrow.field("partition_id", pyarrow.string()),
            ]
        )
        batch = pyarrow.RecordBatch.from_arrays(
            [
                pyarrow.array(
                    [str(channel) for channel in channels],
                    type=schema.field("channel").type,
                ),
                pyarrow.array(dtypes, type=schema.field("data_type").type),
                pyarrow.array(
                    [channel.sample_rate for channel in channels],
                    type=schema.field("sample_rate").type,
                ),
                pyarrow.array(
                    [None for _ in channels],
                    type=schema.field("partition_id").type,
                ),
            ],
            schema=schema,
        )

        # send over the partitions
        writer.begin(schema)
        writer.write_batch(batch)
        writer.done_writing()

        # get back the partition IDs and update
        partitions = reader.read_all().to_pydict()
        for channel, id_ in zip(partitions["channel"], partitions["partition_id"]):
            self._partitions[channel] = id_

    def _get_connection_properties(self, producer_id: str) -> dict[str, str]:
        """Query for producer connection properties."""
        # set up flight info
        descriptor = create_descriptor(
            RequestType.Publish,
            producer_id=producer_id,
        )
        flight_info = self._client.get_flight_info(descriptor)

        # get response, extract connection details
        reader = self._client.do_get(flight_info.endpoints[0].ticket)
        kv_pairs = reader.read_all().column("properties").to_pylist()

        return dict(itertools.chain(*kv_pairs))

    def close(self) -> None:
        super().close()
        try:
            self._producer.flush()
        except Exception:  # noqa: S110
            pass
