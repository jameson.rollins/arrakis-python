# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-python/-/raw/main/LICENSE

from collections.abc import Iterator
from datetime import timedelta
from typing import Protocol, runtime_checkable

import numpy
from typing_extensions import TypeGuard

from . import constants
from .block import SeriesBlock
from .channel import Channel


@runtime_checkable
class Readable(Protocol):
    def fetch(
        self,
        channels: list[str],
        start: int,
        end: int,
    ) -> SeriesBlock:
        ...

    def stream(
        self,
        channels: list[str],
        start: int | None = None,
        end: int | None = None,
    ) -> Iterator[SeriesBlock]:
        ...

    def describe(self, channels: list[str]) -> list[Channel]:
        ...

    def find(
        self,
        pattern: str = constants.DEFAULT_MATCH,
        data_type: list[numpy.dtype] | None = None,
        min_rate: int | None = constants.MIN_SAMPLE_RATE,
        max_rate: int | None = constants.MAX_SAMPLE_RATE,
    ) -> list[Channel]:
        ...

    def count(
        self,
        pattern: str = constants.DEFAULT_MATCH,
        data_type: list[numpy.dtype] | None = None,
        min_rate: int | None = constants.MIN_SAMPLE_RATE,
        max_rate: int | None = constants.MAX_SAMPLE_RATE,
    ) -> int:
        ...


@runtime_checkable
class Writable(Protocol):
    def publish(self, block: SeriesBlock, timeout: timedelta) -> None:
        ...

    def register(self, producer_id: str, source: str, scope: str) -> None:
        ...


def is_readable(client: Readable | Writable) -> TypeGuard[Readable]:
    """Determine if a client supports read-like functionality."""
    return isinstance(client, Readable)


def is_writable(client: Readable | Writable) -> TypeGuard[Writable]:
    """Determine if a client supports write-like functionality."""
    return isinstance(client, Writable)
