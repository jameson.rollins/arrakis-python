# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-python/-/raw/main/LICENSE

"""Arrow Flight utilities."""

from __future__ import annotations

import json
from enum import IntEnum, auto

import pyarrow
from pyarrow import flight

from .channel import Channel


class RequestType(IntEnum):
    Stream = auto()
    Describe = auto()
    Find = auto()
    Count = auto()
    Publish = auto()
    Partition = auto()


def create_command(request_type: RequestType, **kwargs) -> bytes:
    """Create a Flight command containing a JSON-encoded request.

    Parameters
    ----------
    request_type : RequestType
        The type of request.
    **kwargs : dict, optional
        Extra arguments corresponding to the specific request.

    Returns
    -------
    bytes
        The JSON-encoded request.

    """
    cmd = {
        "request": request_type.name,
        "args": kwargs,
    }
    return json.dumps(cmd).encode("utf-8")


def create_descriptor(request_type: RequestType, **kwargs) -> flight.FlightDescriptor:
    """Create a Flight descriptor given a request.

    Parameters
    ----------
    request_type : RequestType
        The type of request.
    **kwargs : dict, optional
        Extra arguments corresponding to the specific request.

    Returns
    -------
    flight.FlightDescriptor
        A Flight Descriptor containing the request.

    """
    cmd = create_command(request_type, **kwargs)
    return flight.FlightDescriptor.for_command(cmd)


def parse_command(cmd: bytes) -> tuple[RequestType, dict]:
    """Parse a Flight command into a request.

    Parameters
    ----------
    cmd : bytes
        The JSON-encoded request.

    Returns
    -------
    request_type : RequestType
        The type of request.
    kwargs : dict
        Arguments corresponding to the specific request.

    """
    parsed = json.loads(cmd.decode("utf-8"))
    return RequestType[parsed["request"]], parsed["args"]


def serialize_batch(batch: pyarrow.RecordBatch):
    """Serialize a record batch to bytes.

    Parameters
    ----------
    batch : pyarrow.RecordBatch
        The batch to serialize.

    Returns
    -------
    bytes
        The serialized buffer.

    """
    sink = pyarrow.BufferOutputStream()
    with pyarrow.ipc.new_stream(sink, batch.schema) as writer:
        writer.write_batch(batch)
    return sink.getvalue()


def channel_to_dtype_name(channel: Channel) -> str:
    """Given a channel, return the data type's name."""
    assert channel.data_type is not None
    return channel.data_type.name


def list_dtype_to_str(dtype: pyarrow.ListType) -> str:
    """Return a string representation of the list's inner data type.

    Note that this does not always match the string representation
    of Arrow's internal data types, to match the behavior across
    different languages.

    Parameters
    ----------
    dtype : pyarrow.ListType
        The list data type to inspect.

    Returns
    -------
    str
        A string representation of the list's inner data type.

    """
    inner_dtype = str(dtype.value_type)
    if inner_dtype == "float":
        return "float32"
    elif inner_dtype == "double":
        return "float64"
    else:
        return inner_dtype
