# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-python/-/raw/main/LICENSE

"""Data block representation of timeseries data."""

from __future__ import annotations

from collections import defaultdict
from collections.abc import Iterator, KeysView
from dataclasses import dataclass, field
from enum import Enum
from functools import cached_property
from numbers import Real

import numpy
import pyarrow
import pyarrow.compute

from .channel import Channel


class Time(int, Enum):
    SECONDS = 1_000_000_000
    MILLISECONDS = 1_000_000
    MICROSECONDS = 1_000
    NANOSECONDS = 1
    s = 1_000_000_000
    ms = 1_000_000
    us = 1_000
    ns = 1


class Freq(Enum):
    GHz = 1_000_000_000
    MHz = 1_000_000
    kHz = 1_000
    Hz = 1

    def __rmul__(self, other: Real) -> int:  # type: ignore
        return int((self.value / other) * Time.s)  # type: ignore


@dataclass(frozen=True)
class Series:
    """Single-channel timeseries data for a given timestamp.

    Parameters
    ----------
    time : int
        The timestamp associated with this data, in nanoseconds.
    data : numpy.ndarray
        The timeseries data.
    duration : float, optional
        The duration of this timeseries, in seconds.
    metadata : Channel, optional
        Channel metadata associated with this timeseries.
        If none is provided, will be generated when requested.

    """

    time: int
    data: numpy.ndarray
    duration: float
    metadata: Channel | str

    @cached_property
    def channel(self) -> Channel:
        if isinstance(self.metadata, str):
            return Channel.from_name(
                self.metadata,
                sample_rate=self.sample_rate,
                data_type=self.data_type,
            )
        return self.metadata

    @property
    def name(self) -> str:
        return str(self.metadata)

    @property
    def t0(self) -> int:
        return self.time

    @property
    def data_type(self) -> numpy.dtype:
        return self.data.dtype

    @property
    def dtype(self) -> numpy.dtype:
        return self.data.dtype

    @cached_property
    def sample_rate(self) -> int:
        if isinstance(self.metadata, str) or self.metadata.sample_rate is None:
            return int(1 / self.duration) * self.data.size
        return self.metadata.sample_rate

    @property
    def dt(self) -> float:
        return 1 / self.sample_rate


@dataclass(frozen=True)
class SeriesBlock:
    """Data block containing timeseries for channels for a given timestamp.

    Parameters
    ----------
    time : int
        The timestamp associated with this data, in nanoseconds.
    data : dict[str, numpy.ndarray]
        Mapping between channels and timeseries.
    duration : float
        The duration of this timeseries, in seconds.
    metadata : dict[str, Channel], optional
        Channel metadata associated with this data block.
        If none is provided, will be generated as requested.

    """

    time: int
    data: dict[str, numpy.ndarray]
    duration: float
    metadata: dict[str, Channel] = field(default_factory=dict)

    @property
    def channels(self) -> list[Channel]:
        """Return all channel metadata associated with this data block.

        Returns
        -------
        list[Channel]
            A list containing channel metadata.

        """
        return [self.channel(channel) for channel in self.data.keys()]

    def channel(self, channel: str) -> Channel:
        """Return the channel metadata associated with a channel.

        Parameters
        ----------
        channel : str
            The channel name.

        Returns
        -------
        Channel
            The channel's metadata.

        """
        if channel not in self.metadata:
            rate = int(1 / self.duration) * self.data[channel].size
            self.metadata[channel] = Channel.from_name(
                channel, sample_rate=rate, data_type=self.data[channel].dtype
            )
        return self.metadata[channel]

    def __getitem__(self, channel: str) -> Series:
        return Series(
            self.time, self.data[channel], self.duration, self.channel(channel)
        )

    def __len__(self) -> int:
        return len(self.data)

    def keys(self) -> KeysView[str]:
        return self.data.keys()

    def items(self) -> dict[str, Series]:
        return {channel: self[channel] for channel in self.keys()}

    def values(self) -> list[Series]:
        return [self[channel] for channel in self.keys()]

    def filter(self, channels: list[str] | None = None) -> SeriesBlock:
        """Filter a block based on criteria.

        Parameters
        ----------
        channels : list[str], optional
            If specified, keep only these channels.

        Returns
        -------
        SeriesBlock
            The filtered series.

        """
        if not channels:
            return self

        data = {channel: self.data[channel] for channel in channels}
        if self.metadata:
            metadata = {channel: self.metadata[channel] for channel in channels}
        else:
            metadata = self.metadata

        return type(self)(self.time, data, self.duration, metadata)

    def to_column_batch(self) -> pyarrow.RecordBatch:
        """Create a row-based record batch from a data block.

        Returns
        -------
        pyarrow.RecordBatch
            A record batch, with a 'time' column with the timestamp
            and channel columns with all channels to publish.

        """
        schema = self._generate_column_schema()
        return pyarrow.RecordBatch.from_arrays(
            [
                pyarrow.array([self.time], type=schema.field("time").type),
                *[
                    pyarrow.array([series], type=schema.field(channel).type)
                    for channel, series in self.data.items()
                ],
            ],
            schema=schema,
        )

    def to_row_batches(self, partitions: dict) -> Iterator[pyarrow.RecordBatch]:
        """Create column-based record batches from a data block.

        Yields
        -------
        pyarrow.RecordBatch
            Record batches, one per data type. The record batches have a
            'time' column with the timestamp, a 'channel' column with
            the channel name, and a 'data' column containing the timeseries.

        """
        # group channels by partitions
        channels_by_part = defaultdict(list)
        for channel in self.keys():
            if channel in partitions:
                partition = partitions[channel]
                channels_by_part[partition].append(channel)

        # generate column-based record batches
        for partition_id, channels in channels_by_part.items():
            # all channels have the same data type
            dtype = self.channel(channels[0]).data_type
            schema = self._generate_row_schema(pyarrow.from_numpy_dtype(dtype))
            series: list[numpy.ndarray] = [self.data[channel] for channel in channels]
            yield partition_id, pyarrow.RecordBatch.from_arrays(
                [
                    pyarrow.array(
                        numpy.full(len(channels), self.time),
                        type=schema.field("time").type,
                    ),
                    pyarrow.array(channels, type=schema.field("channel").type),
                    pyarrow.array(series, type=schema.field("data").type),
                ],
                schema=schema,
            )

    @classmethod
    def from_column_batch(
        cls, batch: pyarrow.RecordBatch, duration: float
    ) -> SeriesBlock:
        """Create a data block from a record batch.

        Parameters
        ----------
        batch : pyarrow.RecordBatch
            A record batch, with a 'time' column with the timestamp
            and channel columns with all channels to publish.
        duration : float
            The duration of this timeseries, in seconds.

        Returns
        -------
        SeriesBlock
            The block representation of the record batch.

        """
        time = batch.column("time").to_numpy()[0]
        fields: list[pyarrow.field] = [field for field in batch.schema]
        channels = [field.name for field in fields[1:]]
        series = {
            channel: pyarrow.compute.list_flatten(batch.column(channel)).to_numpy()
            for channel in channels
        }
        return cls(time, series, duration)

    @classmethod
    def from_row_batch(cls, batch: pyarrow.RecordBatch, duration: float) -> SeriesBlock:
        """Create a data block from a record batch.

        Parameters
        ----------
        batch : pyarrow.RecordBatch
            A record batch, with a 'time' column with the timestamp, a
            'channel' column with the channel name, and a 'data' column
            containing the timeseries.
        duration : float
            The duration of this timeseries, in seconds.

        Returns
        -------
        SeriesBlock
            The block representation of the record batch.

        """
        time = batch.column("time").to_numpy()[0]
        channels = batch.column("channel").to_pylist()
        data = batch.column("data")
        series = {}
        for idx, channel in enumerate(channels):
            series[channel] = pyarrow.array(data[idx]).to_numpy()
        return cls(time, series, duration)

    def _generate_column_schema(self) -> pyarrow.Schema:
        fields = [pyarrow.field("time", pyarrow.int64())]
        for channel, arr in self.data.items():
            dtype = pyarrow.from_numpy_dtype(arr.dtype)
            fields.append(pyarrow.field(channel, pyarrow.list_(dtype)))
        return pyarrow.schema(fields)

    def _generate_row_schema(self, dtype: pyarrow.DataType) -> pyarrow.Schema:
        return pyarrow.schema(
            [
                pyarrow.field("time", pyarrow.int64()),
                pyarrow.field("channel", pyarrow.string()),
                pyarrow.field("data", pyarrow.list_(dtype)),
            ]
        )


# backwards compatibility with previous name
DataBlock = SeriesBlock


def combine_blocks(*blocks: SeriesBlock) -> SeriesBlock:
    """Join a sequence of timeseries blocks into a single block.

    Parameters
    ----------
    *blocks : SeriesBlock
        The timeseries blocks to combine.

    Returns
    -------
    SeriesBlock
        The combined timeseries block.

    """
    time = blocks[0].time
    # FIXME: ensure that all blocks have consistent properties
    duration = blocks[0].duration
    channels = blocks[0].channels
    metadata = blocks[0].metadata
    series: dict[str, numpy.ndarray] = {}
    for channel in map(str, channels):
        series[channel] = numpy.concatenate([block[channel].data for block in blocks])
    return SeriesBlock(time, series, duration, metadata)
